import axios from "axios";
import React from "react";
import { useEffect } from "react";
import { Container } from "react-bootstrap";
import { useLocation, useNavigate } from "react-router";
import "../assets/verification.css";

const Verification = () => {
  const query = useLocation();
  const token = query.search.split("token=")[1];

  const navigate = useNavigate();

  useEffect(() => {
    const verify = async () => {
      try {
        const response = await axios.put("/api/verifyemail", {
          token,
        });

        const data = response.data;

        if (data.status) {
          navigate("/login");
        }
      } catch (error) {
        console.log(error);
      }
    };

    verify();
  }, [token]);

  return (
    <Container
      style={{ height: "100vh" }}
      className="d-flex flex-column justify-content-center align-items-center"
    >
      <h2>Verifikasi Email-mu</h2>
      <p>
        Silakan <strong>klik/ketuk</strong> tombol berikut untuk melakukan
        verifikasi email-mu{" "}
      </p>
      <a href="https://quigley.bix/asperiores-distinctio">
        <button className="btn btn-success ">Verifikasi</button>
      </a>
    </Container>
  );
};

export default Verification;
