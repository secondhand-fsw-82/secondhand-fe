import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import axios from "axios";
import moment from "moment";
import { useState } from "react";
import { useEffect } from "react";
import { Alert, Button, Card, Modal } from "react-bootstrap";
import { useLocation } from "react-router";
import { Link } from "react-router-dom";

const InfoPenawar = () => {
  const location = useLocation();
  const { id } = location.state;
  const [buyer, setBuyer] = useState(null);
  const [product, setProduct] = useState(null);
  const [detail, setDetail] = useState(null);
  const [showModal, setShowModal] = useState(false);
  const [showModalStatus, setShowModalStatus] = useState(false);
  const [showModalCall, setShowModalCall] = useState(false);
  const [alert, setAlert] = useState(null);
  const [showAlert, setShowAlert] = useState(false);
  const [selectStatus, setSelectStatus] = useState("sold");
  const [showAlertStatus, setShowAlertStatus] = useState(false);

  const status = [
    {
      value: "sold",
      label: "Berhasil terjual",
      description: "Kamu telah sepakat menjual produk ini kepada pembeli",
    },
    {
      value: "rejected",
      label: "Batalkan transaksi",
      description: "Kamu membatalkan transaksi produk ini dengan pembeli",
    },
  ];

  useEffect(() => {
    const getOrder = async () => {
      try {
        const response = await axios.get(`/api/ordersid?id=${id}`);
        const data = response.data;

        if (data.status) {
          setBuyer(data.data[0].buyer);
          setProduct(data.data[0].product);
          setDetail(data.data[0]);
        }
      } catch (error) {
        console.log(error);
      }
    };

    getOrder();
  }, [id, alert, showAlertStatus]);

  const rejectOrder = async () => {
    try {
      await axios.put(`/api/order/${id}`, {
        status: "rejected",
      });

      setAlert("deleted");
      setShowAlert(true);
    } catch (error) {
      console.log(error);
    }
  };

  const confirmOrder = async () => {
    try {
      await axios.put(`/api/order/${id}`, {
        status: "waiting",
      });

      setAlert("success");
      setShowAlert(true);
    } catch (error) {
      console.log(error);
    }
  };

  const statusOrder = async () => {
    try {
      await axios.put(`/api/order/${id}`, {
        status: selectStatus,
      });
      setShowAlertStatus(true);
      setShowModalStatus(false);
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <div className="container" style={{ position: "relative" }}>
        <div className="row mt-5">
          <div className="col-md-3 d-flex justify-content-end">
            <Link to="/daftarjual">
              <FontAwesomeIcon icon="fa-arrow-left" />
            </Link>
          </div>

          <div className="col-md-6">
            {/* <div class="alert alert-warning alert-dismissible fade show" role="alert">

                            <strong>Holy guacamole!</strong> You should check in on some of those fields below.

                            <button type="button" class="btn-close" data-bs-dismiss="alert" aria-label="Close"></button>

                        </div> */}
            <Card className="seller" style={{ marginTop: 0 }}>
              <Card.Body>
                {!buyer?.image && (
                  <div className="profile">
                    <FontAwesomeIcon icon="fa-user-alt" size="lg" />
                  </div>
                )}
                <div>
                  <h5 className="card-title" style={{ marginBottom: 4 }}>
                    {buyer?.full_name}
                  </h5>
                  <p className="card-text">
                    {buyer?.city ? buyer.city : "Tidak diketahui"}
                  </p>
                </div>
              </Card.Body>
            </Card>

            <h6 className="title mt-3" style={{ fontWeight: "bold" }}>
              Daftar Produkmu yang Ditawar
            </h6>

            <div className="row mt-3">
              <div className="col-md-2">
                {product?.image.length > 0 ? (
                  <img
                    src={product?.image[0].image}
                    alt=""
                    width={"100%"}
                    style={{ borderRadius: "12px" }}
                  />
                ) : (
                  <div
                    className="card-img-top"
                    style={{
                      height: "100px",
                      objectFit: "contain",
                      display: "flex",
                      alignItems: "center",
                      justifyContent: "center",
                    }}
                  >
                    <FontAwesomeIcon icon="fa-image" size="5x" />
                  </div>
                )}
              </div>

              <div className="col-md-10">
                <div className="row">
                  <div className="col">
                    <span className="card-text">Penawaran produk</span>
                  </div>

                  <div className="col">
                    {/* <span className="card-text float-end">20 Apr, 14:04</span> */}
                    <span className="card-text float-end">
                      {moment(detail?.createdAt).format("DD MMM, h:mm")}
                    </span>
                  </div>
                </div>

                <h5 className="card-title">{product?.name}</h5>

                <h5 className="card-title">
                  {new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "idr",
                  }).format(product?.price)}
                </h5>

                <h5 className="card-title">
                  Ditawar{" "}
                  {new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "idr",
                  }).format(detail?.fix_value)}
                </h5>

                <div className="row mb-3">
                  <div className="col-md-8 offset-md-4 row">
                    {detail?.status === "pending" && (
                      <>
                        <Button
                          onClick={() => setShowModal(true)}
                          className="btn btn-outline-primary col me-2"
                          style={{ color: "black", borderRadius: "1rem" }}
                        >
                          Tolak
                        </Button>

                        <Button
                          onClick={() => confirmOrder()}
                          className="btn btn-primary col"
                          style={{ borderRadius: "1rem" }}
                          data-bs-toggle="modal"
                          data-bs-target="#exampleModalTerima"
                        >
                          Terima
                        </Button>
                      </>
                    )}

                    {detail?.status === "waiting" && (
                      <>
                        <Button
                          onClick={() => setShowModalStatus(true)}
                          className="btn btn-outline-primary col me-2"
                          style={{ color: "black", borderRadius: "1rem" }}
                        >
                          Status
                        </Button>

                        <Button
                          onClick={() => setShowModalCall(true)}
                          className="btn btn-primary col"
                          style={{ borderRadius: "1rem" }}
                          data-bs-toggle="modal"
                          data-bs-target="#exampleModalTerima"
                        >
                          Hubungi di{" "}
                          <FontAwesomeIcon icon="fa-phone" className="ml-2" />
                        </Button>
                      </>
                    )}
                  </div>
                </div>
              </div>

              <hr />
            </div>
          </div>
        </div>

        <Alert
          show={showAlert}
          variant={alert === "success" ? "success" : "danger"}
          className="position-absolute top-0 start-50 translate-middle"
          style={{ zIndex: 10 }}
        >
          <p>
            {alert === "success"
              ? "Tawaran produk diterima"
              : "Tawaran produk ditolak"}
          </p>
          <FontAwesomeIcon
            icon="fa-times"
            style={{ cursor: "pointer" }}
            onClick={() => setShowAlert(false)}
          />
        </Alert>

        <Alert
          show={showAlertStatus}
          variant={"success"}
          className="position-absolute top-0 start-50 translate-middle"
          style={{ zIndex: 10 }}
        >
          <p>Status produk berhasil diperbarui</p>
          <FontAwesomeIcon
            icon="fa-times"
            style={{ cursor: "pointer" }}
            onClick={() => setShowAlertStatus(false)}
          />
        </Alert>
      </div>

      <Modal
        show={showModal}
        onHide={() => setShowModal(false)}
        size="sm"
        centered
      >
        <Modal.Header>
          <Modal.Title>
            Apakah anda yakin akan membatalkan transaksi ini?
          </Modal.Title>
          <Button
            style={{ marginLeft: "auto", padding: "0 .2em" }}
            variant="light"
            onClick={() => setShowModal(false)}
          >
            <FontAwesomeIcon icon="fa-times" />
          </Button>
        </Modal.Header>
        <Modal.Footer>
          <button
            onClick={async () => {
              setShowModal(false);
              rejectOrder();
            }}
            className="btn btn-primary"
          >
            Iya
          </button>
          <button
            onClick={() => {
              setShowModal(false);
            }}
            className="btn btn-outline-primary"
          >
            Tidak
          </button>
        </Modal.Footer>
      </Modal>

      <Modal
        show={showModalCall}
        onHide={() => setShowModalCall(false)}
        size="sm"
        centered
      >
        <Modal.Header>
          <Modal.Title>
            Yeay kamu berhasil mendapat harga yang sesuai
          </Modal.Title>
          <Button
            style={{ marginLeft: "auto", padding: "0 .2em" }}
            variant="light"
            onClick={() => setShowModalCall(false)}
          >
            <FontAwesomeIcon icon="fa-times" />
          </Button>
        </Modal.Header>
        <Modal.Body>
          <p className="card-text">
            Segera hubungi pembeli melalui whatsapp untuk transaksi selanjutnya
          </p>
          <div className="card">
            <div className="card-body">
              <h6 className="text-center mb-3" style={{ fontWeight: "bold" }}>
                Product Match
              </h6>

              <div className="row">
                <div className="col-md-2">
                  {!buyer?.image && (
                    <div className="profile">
                      <FontAwesomeIcon icon="fa-user-alt" size="lg" />
                    </div>
                  )}
                </div>

                <div className="col-md-10">
                  <h5 className="card-title" style={{ fontWeight: "bold" }}>
                    {buyer?.full_name}
                  </h5>

                  <p className="card-text">
                    {buyer?.city ? buyer.city : "Tidak diketahui"}
                  </p>
                </div>
              </div>

              <div className="row mt-3">
                <div className="col-md-2">
                  {product?.image.length > 0 ? (
                    <img
                      src={product?.image[0].image}
                      alt=""
                      width={"100%"}
                      style={{ borderRadius: "12px" }}
                    />
                  ) : (
                    <div
                      className="card-img-top"
                      style={{
                        width: "48px",
                        height: "48px",
                        objectFit: "contain",
                        display: "flex",
                        alignItems: "center",
                        justifyContent: "center",
                      }}
                    >
                      <FontAwesomeIcon icon="fa-image" size="lg" />
                    </div>
                  )}
                </div>

                <div className="col-md-10">
                  <h5 className="card-title">{product?.name}</h5>

                  <h5 className="card-title">
                    <s>
                      {new Intl.NumberFormat("id-ID", {
                        style: "currency",
                        currency: "idr",
                      }).format(product?.price)}
                    </s>
                  </h5>

                  <h5 className="card-title">
                    Ditawar{" "}
                    {new Intl.NumberFormat("id-ID", {
                      style: "currency",
                      currency: "idr",
                    }).format(detail?.fix_value)}
                  </h5>
                </div>
              </div>
            </div>
          </div>
        </Modal.Body>
        <Modal.Footer>
          <Button
            as="a"
            href={"http://wa.me/" + buyer?.phone}
            target="_blank"
            variant="primary"
          >
            Hubungi via Whatsapp{" "}
            <FontAwesomeIcon icon="fa-phone" className="ml-2" />
          </Button>
        </Modal.Footer>
      </Modal>

      <Modal
        show={showModalStatus}
        onHide={() => setShowModalStatus(false)}
        size="sm"
        centered
      >
        <Modal.Header>
          <Modal.Title>Perbarui status penjualan produkmu</Modal.Title>
          <Button
            style={{ marginLeft: "auto", padding: "0 .2em" }}
            variant="light"
            onClick={() => setShowModalStatus(false)}
          >
            <FontAwesomeIcon icon="fa-times" />
          </Button>
        </Modal.Header>
        <Modal.Body>
          <form className="mb-3">
            {status.map((arr) => (
              <div className="form-check">
                <input
                  className="form-check-input"
                  type="radio"
                  name="exampleRadios"
                  value={arr.value}
                  onChange={(e) => setSelectStatus(e.target.value)}
                  checked={arr.value === selectStatus}
                />
                <label className="form-check-label" for="exampleRadios1">
                  {arr.label}
                </label>

                <p className="card-text">{arr.description}</p>
              </div>
            ))}
          </form>
        </Modal.Body>
        <Modal.Footer>
          <Button onClick={() => statusOrder()} variant="primary">
            Kirim
          </Button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default InfoPenawar;
