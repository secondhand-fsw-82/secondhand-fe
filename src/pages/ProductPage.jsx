import React from "react";
import "../assets/style2.css";
import { useNavigate, useParams } from "react-router";
import axios from "axios";
import { useEffect } from "react";
import { useState } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { useDispatch, useSelector } from "react-redux";
import { Alert, Button, Carousel, Modal } from "react-bootstrap";
import { selectedProduct } from "../app/features/productsSlice";

const isPositiveInteger = (str) => {
  if (typeof str !== "string") {
    return false;
  }

  const num = Number(str);

  if (Number.isInteger(num) && num > 0) {
    return true;
  }

  return false;
};

const ProductPage = () => {
  const { id } = useParams();
  const user = useSelector((state) => state.user);
  const [product, setProduct] = useState(null);
  const [tempPrice, setTempPrice] = useState("");
  const [orders, setOrders] = useState(false);
  const [sold, setSold] = useState(false);
  const [showAlert, setShowAlert] = useState(false);
  const [showDanger, setShowDanger] = useState(false);

  const [showModal, setShowModal] = useState(false);

  const navigate = useNavigate();
  const dispatch = useDispatch();

  useEffect(() => {
    const getOrders = async () => {
      try {
        const response = await axios.get(`/api/ordersid?product_id=${id}`);
        const data = response.data;

        if (data.status) {
          const orders = data.data;
          const filterOrders = orders.filter(
            (order) =>
              order.buyer.id === user.user.id && order.status === "pending"
          );

          console.log(filterOrders);

          const isSold = orders.find((order) => order.status === "sold");

          if (isSold) {
            setSold(true);
          }

          if (filterOrders.length > 0) {
            setOrders(true);
          }
        }
      } catch (error) {
        setOrders(false);
        setSold(false);
      }
    };

    const getProductInfo = async () => {
      try {
        const response = await axios.get(`/api/product/${id}`);
        const { data } = response.data;
        setProduct(data);
        getOrders();
      } catch (error) {
        console.log(error);
      }
    };

    getProductInfo();
  }, [id, user.user.id]);

  const buyProduct = () => {
    if (!user.loggedIn) {
      return navigate("/login");
    }

    setShowModal(true);
  };

  const editProduct = () => {
    dispatch(selectedProduct(product));
    navigate("/productinfo");
  };

  const deleteProduct = async () => {
    try {
      const response = await axios.delete(`/api/product/${id}`);
      const data = response.data;

      if (data.status) {
        setShowDanger(true);
        navigate("/");
      }
    } catch (error) {
      console.log(error);
    }
  };

  const publishProduct = async () => {
    try {
      const form = new FormData();
      form.append("name", product.name);
      form.append("price", product.price);
      form.append("category_id", product.category.id);
      form.append("description", product.description);
      form.append("status", "available");
      const response = await axios.put(`/api/product/${id}`, form);
      const data = response.data;
    } catch (error) {
      console.log(error);
    }
  };

  const order = async () => {
    if (!isPositiveInteger(tempPrice)) {
      return false;
    }

    try {
      const response = await axios.post("api/orders", {
        buyer_id: user.user.id,
        product_id: product.id,
        fix_value: tempPrice,
        quantity: 1,
      });

      const data = response.data;

      if (data.status) {
        setOrders(true);
        setShowAlert(true);
        setShowModal(false);
      }
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <>
      <div
        className="container pb-5"
        style={{ marginTop: 40, position: "relative" }}
      >
        <div className="row">
          <div className="col-md-1"></div>
          <div className="col-md-6">
            <Carousel
              fade
              interval={null}
              nextIcon={<FontAwesomeIcon icon="fa-angle-right" />}
              prevIcon={<FontAwesomeIcon icon="fa-angle-left" />}
            >
              {product?.image.length > 0 ? (
                product?.image.map((image, index) => (
                  <Carousel.Item key={image.id}>
                    <img
                      src={image.image}
                      className="d-block w-100 h-100"
                      style={{ objectFit: "contain" }}
                      alt="Gambar produk"
                    />
                  </Carousel.Item>
                ))
              ) : (
                <Carousel.Item
                  style={{
                    height: "100%",
                    objectFit: "contain",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <FontAwesomeIcon icon="fa-image" size="10x" />
                </Carousel.Item>
              )}
            </Carousel>
            <div className="card description mt-5">
              <div className="card-body">
                <h5 className="card-title">Deskripsi</h5>
                <p className="card-text">
                  {product?.description
                    ? product.description
                    : "Tidak ada deskripsi."}
                </p>
              </div>
            </div>
          </div>
          <div className="col-md-4">
            <div className="card product">
              <div className="card-body">
                <h5 className="card-title">{product?.name}</h5>
                <p className="card-text">{product?.category.name}</p>
                <h4 className="card-price">
                  {new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "idr",
                  }).format(product?.price)}
                </h4>

                <div
                  className="d-flex flex-column mt-4"
                  style={{ gap: ".875em" }}
                >
                  {user.user.id === product?.merchant.id ? (
                    <>
                      {sold ? (
                        <Button disabled={sold}>Produkmu sudah terjual</Button>
                      ) : product?.status === "not available" ? (
                        <>
                          <button
                            className="btn btn-primary"
                            onClick={publishProduct}
                          >
                            Terbitakan
                          </button>
                          <button
                            className="btn btn-danger"
                            onClick={deleteProduct}
                          >
                            Hapus
                          </button>
                          <button
                            className="btn btn-outline-primary"
                            onClick={editProduct}
                          >
                            Edit
                          </button>
                        </>
                      ) : (
                        <>
                          <button
                            className="btn btn-danger"
                            onClick={deleteProduct}
                          >
                            Hapus
                          </button>
                          <button
                            className="btn btn-outline-primary"
                            onClick={editProduct}
                          >
                            Edit
                          </button>
                        </>
                      )}
                    </>
                  ) : (
                    <Button onClick={buyProduct} disabled={orders || sold}>
                      {sold
                        ? "Produk sudah terjual"
                        : orders
                        ? "Menunggu respon penjual"
                        : "Saya tertarik dan ingin nego"}
                    </Button>
                  )}
                </div>
              </div>
            </div>
            <div className="card seller">
              <div className="card-body">
                {!product?.merchant.image && (
                  <div className="profile">
                    <FontAwesomeIcon icon="fa-user-alt" size="lg" />
                  </div>
                )}
                {product?.merchant.image && (
                  <img
                    src={product.merchant.image}
                    className="d-block"
                    alt="Seller"
                  />
                )}
                <div>
                  <h5 className="card-title" style={{ marginBottom: 4 }}>
                    {product?.merchant.full_name}
                  </h5>
                  <p className="card-text">
                    {product?.merchant.city
                      ? product.merchant.city
                      : "Tidak diketahui"}
                  </p>
                </div>
              </div>
            </div>
          </div>
          <div className="col-md-1"></div>
        </div>

        <Alert
          show={showAlert}
          variant="success"
          className="position-absolute top-0 start-50 translate-middle"
          style={{ zIndex: 10 }}
        >
          <p>Harga tawarmu berhasil dikirim ke penjual</p>
          <FontAwesomeIcon
            icon="fa-times"
            style={{ cursor: "pointer" }}
            onClick={() => setShowAlert(false)}
          />
        </Alert>
        <Alert
          show={showDanger}
          variant="danger"
          className="position-absolute top-0 start-50 translate-middle"
          style={{ zIndex: 10 }}
        >
          <p>Produk berhasil dihapus</p>
          <FontAwesomeIcon
            icon="fa-times"
            style={{ cursor: "pointer" }}
            onClick={() => setShowDanger(false)}
          />
        </Alert>
      </div>

      <Modal
        show={showModal}
        onHide={() => setShowModal(false)}
        size="sm"
        centered
      >
        <Modal.Header>
          <Modal.Title className="">Masukkan Harga Tawarmu</Modal.Title>
          <Button
            style={{ marginLeft: "auto", padding: "0 .2em" }}
            variant="light"
            onClick={() => setShowModal(false)}
          >
            <FontAwesomeIcon icon="fa-times" />
          </Button>
        </Modal.Header>
        <Modal.Body>
          <p>
            Harga tawaranmu akan diketahui penual, jika penjual cocok kamu akan
            segera dihubungi penjual.
          </p>
          <div className="card product">
            <div className="card-body">
              {product?.image.length > 0 ? (
                <img
                  src={product?.image[0]?.image}
                  className="d-block"
                  alt="Product"
                />
              ) : (
                <div
                  style={{
                    height: "48px",
                    display: "flex",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  <FontAwesomeIcon icon="fa-image" size="lg" />
                </div>
              )}
              <div>
                <h5 className="card-title" style={{ marginBottom: 4 }}>
                  {product?.name}
                </h5>
                <p className="card-text">
                  {" "}
                  {new Intl.NumberFormat("id-ID", {
                    style: "currency",
                    currency: "idr",
                  }).format(product?.price)}
                </p>
              </div>
            </div>
          </div>
          <div className="mt-4 ">
            <label>Harga Tawar</label>
            <input
              type={"text"}
              className="form-control"
              placeholder="Rp 0,00"
              value={tempPrice}
              autoFocus
              onChange={(e) => setTempPrice(e.target.value)}
            />
          </div>
        </Modal.Body>
        <Modal.Footer>
          <button onClick={order} className="btn btn-primary">
            Kirim
          </button>
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default ProductPage;
