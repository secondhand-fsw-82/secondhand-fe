import { createSlice, createAsyncThunk } from "@reduxjs/toolkit";
import axios from "axios";

export const getProducts = createAsyncThunk("products/getProducts", async ({ category }) => {
    const response = await axios.get(`api/product?category=${category}`)
    return response.data
})

const productsSlice = createSlice({
    name: "products",
    initialState: {
        products: [],
        selected: null,
        status: null,
    },
    reducers: {
        selectedProduct: (state, { payload }) => {
            state.selected = payload
        },
        clearSelected: (state, action) => {
            state.selected = null
        }
    },
    extraReducers: {
        [getProducts.pending]: (state, action) => {
            state.status = "loading"
        },
        [getProducts.fulfilled]: (state, { payload }) => {
            state.products = payload
            state.status = "success"
        },
        [getProducts.rejected]: (state, action) => {
            state.status = "failed"
        }
    }
})

export const { selectedProduct, clearSelected } = productsSlice.actions
export default productsSlice.reducer
